
import time
from pilot_cli.servers import WSServer
from pilot_cli.environments import CarEnv, ControllerEnv


controller = ControllerEnv(mode = "client", host = "192.168.1.252", wait_next = False)

i = 0
while True:
    i += 1
    controller.step(
        steering_angle = 0.5,
        throttle = 0.33
    )

    time.sleep(0.5)
    print(i)
