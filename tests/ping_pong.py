
import time
from pilot_cli.servers import WSServer
from pilot_cli.environments import CarEnv, ControllerEnv

server = WSServer()
server.start()

car = CarEnv(mode = "client", wait_next = False)
controller = ControllerEnv(mode = "client", wait_next = False)

d = dict(n = 0)

for i in range(100):

    d = car.step(n = d["n"] + 1)
    d.setdefault("n", 1)
    d["n"] = int(d["n"])
    print("M1", d)

    time.sleep(0.25)

    d = controller.step(n = d["n"] + 1)
    d.setdefault("n", 2)
    d["n"] = int(d["n"])
    print("M2", d)

    time.sleep(0.25)
