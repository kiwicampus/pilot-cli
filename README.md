
## Simulators
###
[Simulators](https://github.com/udacity/self-driving-car-sim)

## Coconut
### Install
```bash
sudo pip install coconut
sudo pip install "coconut[watch]"
```

### Compile
```bash
coconut -w --no-tco pilot-cli
```
