from __future__ import print_function
import numpy as np
import click
import time
from pilot_cli.environments import ControllerEnv, CarEnv
from pilot_cli.extra_environments import ViewerEnv, ListenerEnv
from matplotlib import pyplot as plt
import cv2
import base64, time
from socketIO_client import SocketIO, LoggingNamespace
import pynput
from threading import Thread


def decode_fn(s):
    try:
        s = base64.decodestring(s)
        np_arr = np.fromstring(s, np.uint8)
        return cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
    except Exception as e:
        return str(e)

DATA = {"steering_angle": 0, "throttle": 0}

def on_press0(key):
    incremental = 0.05
    try:
        print('alphanumeric key {0} pressed'.format(
            key.char))
    except AttributeError:
        if key == pynput.keyboard.Key.up:
            DATA["throttle"] += incremental
            print("Up")
        if key == pynput.keyboard.Key.down:
            DATA["throttle"] -= incremental
            #print("down")
        if key == pynput.keyboard.Key.left:
            DATA["steering_angle"] -= incremental
            #print("left")
        if key == pynput.keyboard.Key.right:
            DATA["steering_angle"] += incremental
            # print("right")
        if key == pynput.keyboard.Key.esc:
            return False


def on_press(key):
    incremental = 0.5
    try:
        print('alphanumeric key {0} pressed'.format(
            key.char))
    except AttributeError:
        if key == pynput.keyboard.Key.up:
            DATA["throttle"] = incremental
            print("Up")
        if key == pynput.keyboard.Key.down:
            DATA["throttle"] = -incremental
            #print("down")
        if key == pynput.keyboard.Key.left:
            DATA["steering_angle"] = -incremental
            #print("left")
        if key == pynput.keyboard.Key.right:
            DATA["steering_angle"] = incremental
            # print("right")
        if key == pynput.keyboard.Key.esc:
            return False
    # time.sleep(0.1)

def on_release(key):
    try:
        print('alphanumeric key {0} pressed'.format(
            key.char))
    except AttributeError:
        if key == pynput.keyboard.Key.up:
            #DATA["throttle"] = 0
            print("Up")
        if key == pynput.keyboard.Key.down:
            #DATA["throttle"] = 0
            print("down")
        if key == pynput.keyboard.Key.left:
            DATA["steering_angle"] = 0
            #print("left")
        if key == pynput.keyboard.Key.right:
            DATA["steering_angle"] = 0
            # print("right")
        if key == pynput.keyboard.Key.esc:
            return False

@click.command()
@click.option('--host', '-h', default = "0.0.0.0")
@click.option('--port', '-p', default = "4567")
@click.option('--mode', '-m', default = "server")
@click.option('--dont-wait', is_flag = True)
def main(host, port, mode, dont_wait):
    port = int(port)
    wait_next = not dont_wait

    c = ControllerEnv(mode = "client", host=host, port=port, wait_next = wait_next, decode_fn = decode_fn)

    # thread = Thread(target=thread_fun)
    # thread.daemon = True
    # thread.start()
    with pynput.keyboard.Listener( on_press=on_press0) as listener:
    #with pynput.keyboard.Listener( on_press=on_press, on_release=on_release) as listener:

        while True:
            time1 = time.time()
            data = c.step(steering_angle=DATA["steering_angle"],throttle=DATA["throttle"])
            fps = 1.0/(time.time()-time1)
            if bool(data):
                cv2.imshow('fuck', data['image'])
                cv2.waitKey(1)
            print(fps)
            if fps > 30:
                time.sleep(0.01)



if __name__ == '__main__':
    main()
